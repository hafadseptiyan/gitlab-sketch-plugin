var sketch = require('sketch');
var Settings = require('sketch/settings');
const BrowserWindow = require('sketch-module-web-view');
const fetch = require('sketch-polyfill-fetch')
const FormData = require('sketch-polyfill-fetch/lib/form-data');
const fs = require('@skpm/fs');

export default function(context) {
  //Settings.setSettingForKey('access-token', undefined);
  let win = new BrowserWindow({ width: 800, height: 600, frame: true, title: "Sketch + GitLab" });
  win.webContents.loadURL(require('./export.html'));  
  win.webContents.on('needKey', (key) => {
    if(Settings.settingForKey('access-token')) {
      let name;
      const document = sketch.fromNative(context.document);
      document.selectedLayers.forEach(layer => {
        name = layer.name;
      });
      win.webContents.executeJavaScript('getKey("' + Settings.settingForKey('access-token') + '", "' + name + '", "' + context.document.fileURL().lastPathComponent() + '")').then(res => {
      }).catch(error => {
        console.log(error);
      })
    }else{
      win.webContents.loadURL('https://gitlab.com/oauth/authorize?client_id=24c9502cb351122132a223abeb507897ea05cef51d965a644d1d4ecba5e4835b&redirect_uri=https://gitlab.com&response_type=token&state=test');
    } 
  });

  win.webContents.on('newKey', (key) => {
    win.webContents.loadURL('https://gitlab.com/oauth/authorize?client_id=24c9502cb351122132a223abeb507897ea05cef51d965a644d1d4ecba5e4835b&redirect_uri=https://gitlab.com&response_type=token&state=test');
  });

  win.webContents.on('will-navigate', (event, url) => {
    if(url.substr(0, 7) == 'file://') {
      win.show();
    }  
    let parser = getLocation(url);
    if(parser) {
      parser.href = url;
      if(parser.hostname == 'gitlab.com') {
        if(parser.hash.substr(0,7) == '#access') {
          Settings.setSettingForKey('access-token', parser.hash.replace('#access_token=', '').replace('&token_type=bearer&state=test', ''));
          win.destroy();
          win = new BrowserWindow({ width: 800, height: 600, frame: true, title: "Sketch + GitLab" });
          win.loadURL(require('./export.html'), { extraHeaders: 'pragma: no-cache\n' });
          win.webContents.executeJavaScript('getKey("' + Settings.settingForKey('access-token') + '")').then(res => {
          }).catch(error => {
            console.log(error);
          });
        }
      }else{
        console.log('parser.hostname incorrect or no code');
      }
    }else{
      console.log('no parser');
    }
  });

  win.webContents.on('openTodo', url => {
    NSWorkspace.sharedWorkspace().openURL(NSURL.URLWithString(url))
  });

  win.webContents.on('submitComment', (projectId, issueId, markdown) => {
    exportSelection(projectId, issueId, markdown);
  });

  function exportSelection(projectId, issueId, markdown) {
    const document = sketch.fromNative(context.document);
    document.selectedLayers.forEach(layer => {
      console.log(layer);
      let file = sketch.export(layer, { formats: 'png', overwriting: true });
      var form = new FormData();
      form.append('file', {
        fileName: layer.name + '.png',
        mimeType: 'image/png',
        data: fs.readFileSync('/Users/mvr/Documents/Sketch Exports/' + layer.name + '.png')
      });
      fetch('https://gitlab.com/api/v4/projects/' + projectId + '/uploads', {
        method: 'POST',
        headers: { 'Authorization': 'Bearer ' + Settings.settingForKey('access-token'), 'Content-Type': 'multipart/form-data' },
        body: form
      }).then(response => {
        return response.json()
      }).then(success => {
        fetch('https://gitlab.com/api/v4/projects/' + projectId + '/issues/' + issueId + '/notes?body=' + encodeURI(markdown.replace('[' + layer.name + '](/' + layer.name + '.png)', success.markdown.replace('![', '['))), {
          method: 'POST',
          headers: { 'Authorization': 'Bearer ' + Settings.settingForKey('access-token') }
        }).then(response => {
          return response.json()
        }).then(success => {
          console.log(success);
          win.destroy();
          sketch.UI.message('Comment submitted 🚀');
        }).catch(
          error => console.log(error)
        );
      }).catch(
        error => console.log(error)
      ); 
    });            
  }

  function getLocation(href) {
    var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
      href: href,
      protocol: match[1],
      host: match[2],
      hostname: match[3],
      port: match[4],
      pathname: match[5],
      search: match[6],
      hash: match[7]
    }
  }  
}